/*
 * grunt-gettext-markdown
 * https://github.com/tagoh/grunt-gettext-markdown
 *
 * Copyright (c) 2016 Akira TAGOH
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    jshint: {
      all: ['Gruntfile.js', 'tasks/*.js'],
      options: {
        jshintrc: '.jshintrc'
      }
    },

    // Before generating any new files, remove any previously-created files.
    clean: {
      tests: ['tmp']
    },

    // Configuration to be run (and then tested).
    'gettext-markdown': {
      md2pot: {
        options: {
        },
        files: [
          {src: 'test/fixtures/*.md', dest: 'test/fixtures/po/test.pot'}
        ]
      },
      po2md: {
        options: {
          srcdir: 'test/fixtures',
          validate: true,
          base_path: 'test'
        },
        files: [
          {src: 'test/fixtures/po/*.po', dest: 'test/fixtures/lang/__name__.md.__lang__'},
          {src: 'test/fixtures/po/*.po', dest: 'test/fixtures/path/__path__/__name__.md.__lang__'}
        ]
      }
    }
  });

  // Actually load this plugin's task(s).
  grunt.loadTasks('tasks');

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-readme');

  grunt.registerTask('test', ['clean', 'gettext-markdown']);
  // By default, lint and run all tests.
  grunt.registerTask('default', ['jshint', 'repos', 'readme']);
};
