/*
 * grunt-gettext-markdown
 * https://bitbucket.org/tagoh/grunt-gettext-markdown
 *
 * Copyright (c) 2016 Akira TAGOH
 * Licensed under the MIT license.
 */

'use strict';

const gmd = require('gettext-markdown');
const path = require('path');
const co = require('co');

module.exports = function(grunt) {

  // Please see the grunt documentation for more information regarding task
  // creation: https://github.com/gruntjs/grunt/blob/devel/docs/toc.md

  grunt.registerMultiTask('gettext-markdown', 'Compile PO from markdown/Generate localized markdown from PO', function() {
    let done = this.async();
    let self = this;

    co(function* () {
      // Merge task-specific and/or target-specific options with these defaults.
      let options = self.options({});
      let target = self.target.split('_')[0];

      if (target === 'md2pot') {
        self.files.forEach((f) => {
          let mdfiles = grunt.file.expand({nonull: true}, f.src).filter((ff) => grunt.file.exists(ff));
          let po = gmd.md2pot(mdfiles, options);
          grunt.file.write(f.dest, po);
          grunt.log.writeln(mdfiles.join(',') + ' -> ' + f.dest);
        });
        return true;
      } else if (target === 'po2md') {
        let ret = [];
        let pofiles = [];

        for (let z in self.files) {
          let f = self.files[z];
          let po = grunt.file.expand({nonull: true}, f.src).filter((ff) => grunt.file.exists(ff));
          for (let y in po) {
            let fn = po[y];
            pofiles.push({fn: fn, dest: f.dest});
          }
        }
        for (let z in pofiles) {
          let pofile = pofiles[z];
          let po = grunt.file.read(pofile.fn);
          ret.push(yield gmd.po2md(pofile.fn, po, options)
                   .then((r) => {
                     return co(function* () {
                       let retx = [];
                       for (let i in r) {
                         let fn = r[i].fn;
                         let pofn = r[i].po;
                         let lang = r[i].lang;
                         let data = r[i].data;
                         let fx = () => {
                           let name = path.basename(fn, path.extname(fn));
                           let dir = path.dirname(fn);
                           if (options.base_path) {
                             dir = dir.replace(new RegExp('^' + options.base_path), '');
                           }
                           let dest = pofile.dest.replace(/__path__/g, dir).replace(/__name__/g, name).replace(/__lang__/g, lang);
                           grunt.file.write(dest, data);
                           grunt.log.writeln(pofile.fn + ' -> ' + dest);
                         };
                         if (options.validate) {
                           retx.push(yield gmd.validate(fn)
                                     .then((resolve) => {
                                       fx();
                                       return Promise.resolve(true);
                                     }, (reject) => {
                                       return Promise.reject(new Error('Unable to parse ' + fn));
                                     }));
                         } else {
                           fx();
                           retx.push(Promise.resolve(true));
                         }
                       }
                       return retx;
                     });
                   }));
        }
        return ret;
      } else {
        grunt.log.error('Invalid target: ' + self.target);
        return false;
      }
    })
      .then((v) => done(true))
      .catch((e) => {
        grunt.log.error(e);
        done(false);
      });
  });

};
